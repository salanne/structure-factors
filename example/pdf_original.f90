program pdf

implicit none

integer :: i,j,jj
integer :: ntype,nattot,npairs,nbinr,nbink
integer, allocatable, dimension(:,:) :: nattype
double precision :: du
double precision :: deltar,rmax
double precision :: deltak,kmax,kvalue
double precision :: boxlenx,boxleny,boxlenz,cellvol,pi,totalrho
double precision :: weight, ea,cc1,cc2,theta
double precision :: frac1,frac2,f1,f2
double precision, dimension(4) :: ac1,bc1,ac2,bc2
double precision, allocatable, dimension(:) :: rdf,rvalue,pdfunc,rdftot,rdftotnorm,pdfunc2
double precision, allocatable, dimension(:) :: intensity,correc,intensity2,correc2
double precision, allocatable, dimension(:,:) :: sk,contrib
character*2, allocatable, dimension(:,:) :: element
character*80, allocatable, dimension(:) :: rdffile,sqfile

open(10,file='pdf.inpt')
read(10,*)ntype,nattot
npairs=ntype*(ntype+1)/2
allocate(element(npairs,2))
allocate(nattype(npairs,2))
allocate(rdffile(npairs))
allocate(sqfile(npairs))
do i=1,npairs
   read(10,*)element(i,1),element(i,2)
   read(10,*)nattype(i,1),nattype(i,2)
   read(10,'(a)')rdffile(i)
   read(10,'(a)')sqfile(i)
enddo
read(10,*)boxlenx,boxleny,boxlenz
read(10,*)deltar,rmax
read(10,*)deltak,kmax
close(10)

cellvol=boxlenx*boxleny*boxlenz
totalrho=nattot/cellvol
pi=4.0d0*atan(1.0d0)
nbinr=int(rmax/deltar)
nbink=int(kmax/deltak)
! Add +1 if the first point is at r=0
allocate(rdf(nbinr),rvalue(nbinr))
allocate(rdftot(nbinr))
allocate(rdftotnorm(nbinr))
allocate(sk(npairs,nbink))
allocate(contrib(npairs,nbink))
allocate(intensity(nbink),correc(nbink))
allocate(intensity2(nbink),correc2(nbink))
sk=0.d0
rdftot=0.d0
rdftotnorm=0.d0

intensity=0.d0
contrib=0.d0
intensity2=0.d0
correc=0.d0
correc2=0.d0

do i=1,npairs
   open(10+i,file=rdffile(i))
   do j=1,nbinr
      read(10+i,*)rvalue(j),rdf(j)
      if(element(i,1).eq.element(i,2))then
            rdftot(j)=rdftot(j)+rdf(j)*nattype(i,1)*nattype(i,1)/2.0
            rdftotnorm(j)=rdftotnorm(j)+nattype(i,1)*nattype(i,1)/2.0
      else
            rdftot(j)=rdftot(j)+rdf(j)*nattype(i,1)*nattype(i,2)
            rdftotnorm(j)=rdftotnorm(j)+nattype(i,1)*nattype(i,2)
      endif
   enddo
   close(10+i)
   do j=1,nbink
      kvalue=dble(j)*deltak
      do jj=1,nbinr
!        rvalue=dble(jj)*deltar
! Change for jj=1 if the first point is at r=0
         sk(i,j)=sk(i,j)+rvalue(jj)*sin(kvalue*rvalue(jj))*(rdf(jj)-1.0d0)/kvalue
      enddo
!     sk(i,j)=4.0*pi*dsqrt(dble(nattype(i,1)*nattype(i,2)))*sk(i,j)*deltar/cellvol
      sk(i,j)=4.0*pi*totalrho*sk(i,j)*deltar
      if(element(i,1).eq.element(i,2))sk(i,j)=sk(i,j)+1.0d0

      ac1(1)=0.d0
      ac1(2)=0.d0
      ac1(3)=0.d0
      ac1(4)=0.d0
      bc1(1)=0.d0
      bc1(2)=0.d0
      bc1(3)=0.d0
      bc1(4)=0.d0
      cc1=0.d0
      ac2(1)=0.d0
      ac2(2)=0.d0
      ac2(3)=0.d0
      ac2(4)=0.d0
      bc2(1)=0.d0
      bc2(2)=0.d0
      bc2(3)=0.d0
      bc2(4)=0.d0
      cc2=0.d0
      select case (element(i,1))
         case("H")
           ac1(1)=0.489918
           ac1(2)=0.262003
           ac1(3)=0.196767
           ac1(4)=0.049879
           bc1(1)=20.6593
           bc1(2)=7.74039
           bc1(3)=49.5519
           bc1(4)=2.20159
           cc1=0.001305
         case("C")
           ac1(1)=2.31000
           ac1(2)=1.02000
           ac1(3)=1.58860
           ac1(4)=0.865000
           bc1(1)=20.8439
           bc1(2)=10.2075
           bc1(3)=0.568700
           bc1(4)=51.6512
           cc1=0.215600
         case("N")
           ac1(1)=12.2126
           ac1(2)=3.13220
           ac1(3)=2.01250
           ac1(4)=1.16630
           bc1(1)=0.005700
           bc1(2)=9.89330
           bc1(3)=28.9975
           bc1(4)=0.582600
           cc1=-11.529
         case("O")
           ac1(1)=3.04850
           ac1(2)=2.28680
           ac1(3)=1.54630
           ac1(4)=0.867000
           bc1(1)=13.2771
           bc1(2)=5.70110
           bc1(3)=0.323900
           bc1(4)=32.9089
           cc1=0.250800
         case("Li")
           ac1(1)=0.6968
           ac1(2)=0.7888
           ac1(3)=0.3414
           ac1(4)=0.1563
           bc1(1)=4.6237
           bc1(2)=1.9557
           bc1(3)=0.6316
           bc1(4)=10.0953
           cc1=0.0167
         case("Cl")
           ac1(1)=11.4604
           ac1(2)=7.1964
           ac1(3)=6.2556
           ac1(4)=1.6455
           bc1(1)=0.0104
           bc1(2)=1.1662
           bc1(3)=18.5194
           bc1(4)=47.7784
           cc1=-9.5574
! Need to check the coefficients below before uncommenting!!!
!        case("X")
! O2-
!          ac1(1)=3.75040
!          bc1(1)=2.84294
!          ac1(2)=1.54298
!          bc1(2)=1.62091
!          ac1(3)=16.5151
!          bc1(3)=6.59203
!          ac1(4)=0.319201
!          bc1(4)=43.3486
!          cc1=0.242060
!        case("Y")
! F-
!          ac1(1)=3.63220
!          bc1(1)=3.51057
!          ac1(2)=1.26064
!          bc1(2)=0.940706
!          ac1(3)=5.27756
!          bc1(3)=14.7353
!          ac1(4)=0.442258
!          bc1(4)=47.3437
!          cc1=0.653396
!        case("T")
! Ti4+
!          ac1(1)=19.5114
!          bc1(1)=8.23473
!          ac1(2)=2.01341
!          bc1(2)=1.52080
!          ac1(3)=0.178847
!          bc1(3)=6.67018
!          ac1(4)=-0.29263
!          bc1(4)=12.9464
!          cc1=-13.280
         case("Z")
           ac1(1)=0.00000
           bc1(1)=8.23473
           ac1(2)=0.00000
           bc1(2)=1.52080
           ac1(3)=0.00000
           bc1(3)=6.67018
           ac1(4)=0.00000
           bc1(4)=12.9464
           cc1=1.0
         case("A")
           ac1(1)=0.00000
           bc1(1)=8.23473
           ac1(2)=0.00000
           bc1(2)=1.52080
           ac1(3)=0.00000
           bc1(3)=6.67018
           ac1(4)=0.00000
           bc1(4)=12.9464
           cc1=0.0
      end select
      select case (element(i,2))
         case("H")
           ac2(1)=0.489918
           ac2(2)=0.262003
           ac2(3)=0.196767
           ac2(4)=0.049879
           bc2(1)=20.6593
           bc2(2)=7.74039
           bc2(3)=49.5519
           bc2(4)=2.20159
           cc2=0.001305
         case("C")
           ac2(1)=2.31000
           ac2(2)=1.02000
           ac2(3)=1.58860
           ac2(4)=0.865000
           bc2(1)=20.8439
           bc2(2)=10.2075
           bc2(3)=0.568700
           bc2(4)=51.6512
           cc2=0.215600
         case("N")
           ac2(1)=12.2126
           ac2(2)=3.13220
           ac2(3)=2.01250
           ac2(4)=1.16630
           bc2(1)=0.005700
           bc2(2)=9.89330
           bc2(3)=28.9975
           bc2(4)=0.582600
           cc2=-11.529
         case("O")
           ac2(1)=3.04850
           ac2(2)=2.28680
           ac2(3)=1.54630
           ac2(4)=0.867000
           bc2(1)=13.2771
           bc2(2)=5.70110
           bc2(3)=0.323900
           bc2(4)=32.9089
           cc2=0.250800
         case("Li")
           ac2(1)=0.6968
           ac2(2)=0.7888
           ac2(3)=0.3414
           ac2(4)=0.1563
           bc2(1)=4.6237
           bc2(2)=1.9557
           bc2(3)=0.6316
           bc2(4)=10.0953
           cc2=0.0167
         case("Cl")
           ac2(1)=11.4604
           ac2(2)=7.1964
           ac2(3)=6.2556
           ac2(4)=1.6455
           bc2(1)=0.0104
           bc2(2)=1.1662
           bc2(3)=18.5194
           bc2(4)=47.7784
           cc2=-9.5574
! Check!!!!!!!!
!        case("X")
! O2-
!          ac2(1)=3.75040
!          bc2(1)=2.84294
!          ac2(2)=1.54298
!          bc2(2)=1.62091
!          ac2(3)=16.5151
!          bc2(3)=6.59203
!          ac2(4)=0.319201
!          bc2(4)=43.3486
!          cc2=0.242060
!        case("Y")
! F-
!          ac2(1)=3.63220
!          bc2(1)=3.51057
!          ac2(2)=1.26064
!          bc2(2)=0.940706
!          ac2(3)=5.27756
!          bc2(3)=14.7353
!          ac2(4)=0.442258
!          bc2(4)=47.3437
!          cc2=0.653396
!        case("T")
! Ti4+
!          ac2(1)=19.5114
!          bc2(1)=8.23473
!          ac2(2)=2.01341
!          bc2(2)=1.52080
!          ac2(3)=0.178847
!          bc2(3)=6.67018
!          ac2(4)=-0.29263
!          bc2(4)=12.9464
!          cc2=-13.280
         case("Z")
           ac2(1)=0.00000
           bc2(1)=8.23473
           ac2(2)=0.00000
           bc2(2)=1.52080
           ac2(3)=0.00000
           bc2(3)=6.67018
           ac2(4)=0.00000
           bc2(4)=12.9464
           cc2=1.0
         case("A")
           ac2(1)=0.00000
           bc2(1)=8.23473
           ac2(2)=0.00000
           bc2(2)=1.52080
           ac2(3)=0.00000
           bc2(3)=6.67018
           ac2(4)=0.00000
           bc2(4)=12.9464
           cc2=0.0
      end select
      frac1=dble(nattype(i,1))/dble(nattot)
      frac2=dble(nattype(i,2))/dble(nattot)
      theta=kvalue/(4.0*pi)

      f1=0.d0
      do jj=1,4
        ea=-bc1(jj)*theta*theta
        f1=f1+ac1(jj)*exp(ea)
      enddo
      f1=f1+cc1

      f2=0.d0
      do jj=1,4
        ea=-bc2(jj)*theta*theta
        f2=f2+ac2(jj)*exp(ea)
      enddo
      f2=f2+cc2
!     weight=f1*f2*dsqrt(frac1*frac2)
!     if(element(i,1).eq.element(i,2))weight=weight*2.0d0
      if(element(i,1).eq.element(i,2))then
         weight=f1*f2*frac1*frac2
!        write(200+i,*)kvalue,f1,f2
!        write(300+i,*)kvalue,weight
         correc(j)=correc(j)+f1*frac1
         correc2(j)=correc2(j)+frac1
         intensity(j)=intensity(j)+(sk(i,j)-1)*weight
         contrib(i,j)=(sk(i,j)-1)*weight
         intensity2(j)=intensity2(j)+(sk(i,j)-1)*frac1*frac2
      else
!        write(200+i,*)kvalue,f1,f2
!        write(300+i,*)kvalue,weight
         weight=2.0*f1*f2*frac1*frac2
         intensity(j)=intensity(j)+(sk(i,j))*weight
         contrib(i,j)=(sk(i,j))*weight
         intensity2(j)=intensity2(j)+(sk(i,j))*2.0*frac1*frac2
      endif

   enddo
   close(10+i)
enddo

allocate(pdfunc(nbinr))
allocate(pdfunc2(nbinr))
pdfunc=0.d0
open(51,file='pdf.out')
write(51,*)'#First column: r, Second column: PDF with f(k), Third column: PDF with f(k)=1'
do i=1,nbinr
   do j=1,nbink
      kvalue=dble(j)*deltak
      pdfunc(i)=pdfunc(i)+kvalue*(intensity(j)/correc(j)**2.0)*sin(kvalue*rvalue(i))
      pdfunc2(i)=pdfunc2(i)+kvalue*(intensity2(j)/correc2(j)**2.0)*sin(kvalue*rvalue(i))
   enddo
   write(51,*)rvalue(i),pdfunc(i)*deltak*2.0/pi,pdfunc2(i)*deltak*2.0/pi
enddo
close(51)

open(50,file='intensity.out')
open(53,file='contrib-structurefactor.out')
write(53,*)'#Column',1,': q'
write(53,*)'#Column',2,': Total S(q)-1'
do i=1,npairs
   open(110+i,file=sqfile(i))
   write(53,*)'#Column',2+i,': contrib from pair',element(i,1),element(i,2)
enddo
do j=1,nbink
   write(50,*)deltak*j,1+intensity(j)/correc(j)**2.0,intensity(j),correc(j)
   write(53,*)deltak*j,intensity(j)/correc(j)**2.0,(contrib(i,j)/correc(j)**2.0,i=1,npairs)
   do i=1,npairs
      write(110+i,*)deltak*j,sk(i,j)
   enddo
enddo
close(50)
close(53)
do i=1,npairs
   close(110+i)
enddo

open(52,file='rdftot.out')
do j=1,nbinr
   write(52,*)rvalue(j),rdftot(j)/rdftotnorm(j)
enddo
close(52)

end program
