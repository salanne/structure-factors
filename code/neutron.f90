program neutron

implicit none

! Neutron scattering lengths taken from
! https://www.ncnr.nist.gov/resources/n-lengths/

integer :: i,j,jj
integer :: ntype,nattot,npairs,nbinr,nbink
integer, allocatable, dimension(:,:) :: nattype
double precision :: du
double precision :: deltar,rmax
double precision :: deltak,kmax,kvalue
double precision :: boxlenx,boxleny,boxlenz,cellvol,pi,totalrho
double precision :: weight, ea,cohlength1,cohlength2
double precision :: frac1,frac2,f1,f2
double precision, allocatable, dimension(:) :: rdf,rvalue,neutronfunc,rdftot,rdftotnorm
double precision, allocatable, dimension(:) :: intensity,correc,totalsq
double precision, allocatable, dimension(:) :: correctotalsq
double precision, allocatable, dimension(:,:) :: sk,contrib
character*5, allocatable, dimension(:,:) :: element
character*80, allocatable, dimension(:) :: rdffile,sqfile

open(10,file='neutron.inpt')
read(10,*)ntype,nattot
npairs=ntype*(ntype+1)/2
allocate(element(npairs,2))
allocate(nattype(npairs,2))
allocate(rdffile(npairs))
allocate(sqfile(npairs))
do i=1,npairs
   read(10,*)element(i,1),element(i,2)
   read(10,*)nattype(i,1),nattype(i,2)
   read(10,'(a)')rdffile(i)
   read(10,'(a)')sqfile(i)
enddo
read(10,*)boxlenx,boxleny,boxlenz
read(10,*)deltar,rmax
read(10,*)deltak,kmax
close(10)

cellvol=boxlenx*boxleny*boxlenz
totalrho=nattot/cellvol
pi=4.0d0*atan(1.0d0)
nbinr=int(rmax/deltar)
nbink=int(kmax/deltak)
! Add +1 if the first point is at r=0
allocate(rdf(nbinr),rvalue(nbinr))
allocate(rdftot(nbinr))
allocate(rdftotnorm(nbinr))
allocate(sk(npairs,nbink))
allocate(contrib(npairs,nbink))
allocate(intensity(nbink),correc(nbink))
allocate(totalsq(nbink))
allocate(correctotalsq(nbink))
sk=0.d0
rdftot=0.d0
rdftotnorm=0.d0

intensity=0.d0
contrib=0.d0
correc=0.d0
totalsq=0.d0
correctotalsq=0.d0

do i=1,npairs
   open(10+i,file=rdffile(i))
   do j=1,nbinr
      read(10+i,*)rvalue(j),rdf(j)
      if(element(i,1).eq.element(i,2))then
            rdftot(j)=rdftot(j)+rdf(j)*nattype(i,1)*nattype(i,1)/2.0
            rdftotnorm(j)=rdftotnorm(j)+nattype(i,1)*nattype(i,1)/2.0
      else
            rdftot(j)=rdftot(j)+rdf(j)*nattype(i,1)*nattype(i,2)
            rdftotnorm(j)=rdftotnorm(j)+nattype(i,1)*nattype(i,2)
      endif
   enddo
   close(10+i)
   do j=1,nbink
      kvalue=dble(j)*deltak
      do jj=1,nbinr
!        rvalue=dble(jj)*deltar
! Change for jj=1 if the first point is at r=0
         sk(i,j)=sk(i,j)+rvalue(jj)*sin(kvalue*rvalue(jj))*(rdf(jj)-1.0d0)/kvalue
      enddo
!     sk(i,j)=4.0*pi*dsqrt(dble(nattype(i,1)*nattype(i,2)))*sk(i,j)*deltar/cellvol
      sk(i,j)=4.0*pi*totalrho*sk(i,j)*deltar
      if(element(i,1).eq.element(i,2))sk(i,j)=sk(i,j)+1.0d0

      cohlength1=0.d0
      cohlength2=0.d0
      select case (element(i,1))
         case("NatMg")
           cohlength1=5.375
         case("24Mg")  
           cohlength1=5.66
         case("25Mg")  
           cohlength1=3.62
         case("26Mg")  
           cohlength1=4.89
         case("NatCl")
           cohlength1=9.577
         case("35Cl")  
           cohlength1=11.65
         case("37Cl")  
           cohlength1=3.08
      end select
      select case (element(i,2))
         case("NatMg")
           cohlength2=5.375
         case("24Mg")  
           cohlength2=5.66
         case("25Mg")  
           cohlength2=3.62
         case("26Mg")  
           cohlength2=4.89
         case("NatCl")
           cohlength2=9.577
         case("35Cl")  
           cohlength2=11.65
         case("37Cl")  
           cohlength2=3.08
      end select
      frac1=dble(nattype(i,1))/dble(nattot)
      frac2=dble(nattype(i,2))/dble(nattot)

      f1=cohlength1

      f2=cohlength2

      if(element(i,1).eq.element(i,2))then
         weight=f1*f2*frac1*frac2
!        correc(j)=correc(j)+f1*frac1
         intensity(j)=intensity(j)+(sk(i,j)-1)*weight
         contrib(i,j)=(sk(i,j)-1)*weight
!        totalsq(j)=totalsq(j)+sk(i,j)*weight
!        correctotalsq(j)=correctotalsq(j)+weight
      else
         weight=2.0*f1*f2*frac1*frac2
         intensity(j)=intensity(j)+(sk(i,j))*weight
         contrib(i,j)=(sk(i,j))*weight
!        totalsq(j)=totalsq(j)+sk(i,j)*weight
      endif

   enddo
   close(10+i)
enddo

!open(50,file='neutron-intensity.out')
open(53,file='neutron-structurefactor.out')
write(53,*)'#Column',1,': q'
write(53,*)'#Column',2,': Total F(q)'
do i=1,npairs
   open(110+i,file=sqfile(i))
   write(53,*)'#Column',2+i,': contrib from pair',element(i,1),element(i,2)
enddo
do j=1,nbink
!  write(50,*)deltak*j,1+intensity(j)/correc(j)**2.0,intensity(j),correc(j)
   write(53,*)deltak*j,intensity(j),(contrib(i,j),i=1,npairs)
   do i=1,npairs
      write(110+i,*)deltak*j,sk(i,j)
   enddo
enddo
close(50)
close(53)
do i=1,npairs
   close(110+i)
enddo

end program
