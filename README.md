# Structure factors

Analysis tool to compute partial and total (X-ray or neutron) structure factors from radial distribution functions

Compilation
```bash
gfortran pdf.f90 -o pdf.x
```

Usage
```bash
./pdf.x
```

It needs an input file named *pdb.inpt*. An example taken from [[Dubouis2020][1]] is given in the folder *example*



Reference

[1]:https://www.nature.com/articles/s41929-020-0482-5
N. Dubouis, A. Serva, R. Berthin, G. Jeanmairet, B. Porcheron, E. Salager, M. Salanne and A. Grimaud.
Tuning the Water Reduction Through Controlled Nanoconfinement Within an Organic Liquid Matrix.
Nature Catalysis, 3, 656--663 (2020)
